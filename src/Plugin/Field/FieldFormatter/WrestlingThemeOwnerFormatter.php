<?php

namespace Drupal\wrestling_themes_manager\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'wrestling_theme_owner' formatter.
 *
 * @FieldFormatter(
 *   id = "wrestling_theme_owner",
 *   label = @Translation("Wrestling theme owner"),
 *   description = @Translation("Display the label of the Wrestling theme owner node."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class WrestlingThemeOwnerFormatter extends EntityReferenceLabelFormatter implements ContainerFactoryPluginInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings']);

    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * Hide the value of "field_owner" if a wrestler is owner of a theme.
   *
   * The current wrestler is the wrestler whose node is being viewed.
   * We get the wrestler's node id from the current route.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $theme_owner_id = $items->first()->target_id;
    $wrestler_id = $this->routeMatch->getRawParameter('node');

    if ($wrestler_id && $wrestler_id === $theme_owner_id) {
      // Disable caching to prevent the issue where all "field_owner" fields
      // are not render until rebuilding the cache.
      $elements['#cache']['max-age'] = 0;
      return $elements;
    }

    return parent::viewElements($items, $langcode);
  }

}
